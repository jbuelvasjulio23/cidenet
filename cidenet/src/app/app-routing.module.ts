import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from '../app/componentes/inicio/inicio.component';
import { ConsultarComponent } from '../app/componentes/consultar/consultar.component';
import { RegistrarComponent } from '../app/componentes/registrar/registrar.component';

const routes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'consultar', component: ConsultarComponent },
  { path: 'registrar', component: RegistrarComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
