export interface Empleado {
    id : String,
    p_nombre : String,
    s_nombre : String,
    p_apellido : String,
    s_apellido : String,
    pais : String,
    t_documento : String,
    n_documento : String,
    email : String,
    f_ingreso : String,
    area : String,
    estado : String,
    f_hora_registro : String
}