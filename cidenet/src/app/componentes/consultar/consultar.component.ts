import { Component, OnInit } from '@angular/core';
import { RegistroServiceService } from '../../servicios/registro-service.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2'
import * as $ from 'jquery';

@Component({
  selector: 'app-consultar',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent implements OnInit {
  lista: any;
  filtro: FormGroup;
  id: any;
  indice: String;
  empleado: FormGroup;
  fecha_actual: Date;
  primero: number;
  ultimo: number;
  tam_registros: number;
  pag_Actual: number;

  sigButton: Boolean;
  antButton: Boolean;
  habilitar : Boolean;


  constructor(private fb: FormBuilder, private registroService: RegistroServiceService) {
    this.lista = null;
    this.indice = "";
    this.fecha_actual = new Date();
    this.primero = 0;
    this.ultimo = 10;
    this.tam_registros = 0;
    this.pag_Actual = 1;

    this.sigButton = false;
    this.antButton = false;
    this.habilitar = false;   

    this.filtro = this.fb.group({
      p_apellido: ['', [Validators.maxLength(20)]],
      s_apellido: ['', [Validators.maxLength(20)]],
      p_nombre: ['', [Validators.maxLength(20)]],
      s_nombre: ['', [Validators.maxLength(50)]],
      pais: ['', []],
      estado: ['', []],
      area: ['', []],
      tipo_id: ['', []],
      n_identificacion: ['', [Validators.maxLength(20)]],
      email: ['', [Validators.email]],
    });

    this.empleado = this.fb.group({
      p_apellido: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern('[a-zA-Z]*')]],
      s_apellido: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern('[a-zA-Z]*')]],
      p_nombre: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern('[a-zA-Z]*')]],
      s_nombre: ['', [Validators.minLength(4), Validators.maxLength(50), Validators.pattern('[a-zA-Z ]*')]],
      pais: ['', [Validators.required]],
      area: ['', [Validators.required]],
      tipo_id: ['', [Validators.required]],
      n_identificacion: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20), Validators.pattern('[a-zA-Z0-9]*')]],
      f_ingreso: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.filtrar();
  }

  estructuraEmpleado(){
    const empleado = {
      id: "",
      p_nombre: this.filtro.value.p_nombre.toUpperCase() + "%",
      s_nombre: this.filtro.value.s_nombre.toUpperCase() + "%",
      p_apellido: this.filtro.value.p_apellido.toUpperCase() + "%",
      s_apellido: this.filtro.value.s_apellido.toUpperCase() + "%",
      pais: this.filtro.value.pais + "",
      t_documento: this.filtro.value.tipo_id + "",
      n_documento: this.filtro.value.n_identificacion.toUpperCase() + "%",
      email: this.filtro.value.email + "%",
      f_ingreso: "",
      area: this.filtro.value.area + "",
      estado: this.filtro.value.estado + "",
      f_hora_registro: ""
    }
    return empleado;
  }

  async cantidadRegistros(){
    let resp: any = await this.registroService.mostrarCantidad(this.estructuraEmpleado()).toPromise();
    this.tam_registros = resp.email;
  }

  async filtrar(){
    this.habilitar = true;
    await this.cantidadRegistros();
    await this.mostrar();
    this.habilitar = false;
  }

  async mostrar() {
    if ( this.tam_registros > 0) {
      let resp : any = await this.registroService.mostrarEmpleado(this.estructuraEmpleado(), this.primero, this.ultimo).toPromise();
      this.lista = resp;
    } else {
      this.lista = null;
    }

    if (this.tam_registros <= this.pag_Actual * 10) {
      this.sigButton = true;
    }else{
      this.sigButton = false;
    }

    if (this.pag_Actual == 1) {
      this.antButton = true;
    }else{
      this.antButton = false;
    }
  }

  async eliminar() {
    let resp: any = await this.registroService.eliminarEmpleado(this.id).toPromise();
    this.alerta("success",resp.email, "Eliminado!");
    this.filtrar();
    $('#confirmarModalEliminar').click();
  }

  confirmarEliminacion(id: any) {
    this.id = id;
  }

  editar(item: any) {
    this.indice = item;
    this.pasarDatos();
  }

  async actualizar() {
    var fecha = this.empleado.value.f_ingreso.split("/");
    var f = new Date(fecha[2], fecha[1] - 1, fecha[0]);
    var f2 = new Date();
    f2.setMonth(f2.getMonth() - 1);

    if (f > this.fecha_actual) {
      this.alerta("error","La fecha no puede ser mayor a la actual", "Edicion");
    } else if (f <= this.fecha_actual && f > f2) {
      if (this.empleado.valid) {
        const empleado = {
          id: this.lista[this.indice + ""].id,
          p_nombre: this.empleado.value.p_nombre.toUpperCase(),
          s_nombre: this.empleado.value.s_nombre.toUpperCase(),
          p_apellido: this.empleado.value.p_apellido.toUpperCase(),
          s_apellido: this.empleado.value.s_apellido.toUpperCase(),
          pais: this.empleado.value.pais + "",
          t_documento: this.empleado.value.tipo_id + "",
          n_documento: this.empleado.value.n_identificacion.toUpperCase(),
          email: this.lista[this.indice + ""].email,
          f_ingreso: this.empleado.value.f_ingreso,
          area: this.empleado.value.area + "",
          estado: "",
          f_hora_registro: ""
        }
        let response: any = await this.registroService.editarEmpleado(empleado).toPromise();
        this.alerta("success",response.email, "Edicion");
        $('#btn-cancelarModal').click();
        this.filtrar();
      }
    } else {
      this.alerta("error","la fecha no puede superar un mes de antiguedad", "Edicion");
    }
  }

  pasarDatos() {
    this.empleado.controls['p_apellido'].setValue(this.lista[this.indice + ""].p_apellido);
    this.empleado.controls['s_apellido'].setValue(this.lista[this.indice + ""].s_apellido);
    this.empleado.controls['p_nombre'].setValue(this.lista[this.indice + ""].p_nombre);
    this.empleado.controls['s_nombre'].setValue(this.lista[this.indice + ""].s_nombre);
    this.empleado.controls['n_identificacion'].setValue(this.lista[this.indice + ""].n_documento);
    this.empleado.controls['f_ingreso'].setValue(this.lista[this.indice + ""].f_ingreso);
    this.empleado.controls['pais'].setValue(this.pais(this.lista[this.indice + ""].pais));
    this.empleado.controls['area'].setValue(this.area(this.lista[this.indice + ""].area));
    this.empleado.controls['tipo_id'].setValue(this.tipo_documento(this.lista[this.indice + ""].t_documento));
  }

  pagSiguiente() {
    this.primero = this.primero + 10;
    //this.ultimo = this.ultimo + 10;
    this.pag_Actual++;
    this.mostrar();
  }

  pagAnterior() {
    this.primero = this.primero - 10;
    //this.ultimo = this.ultimo - 10;
    this.pag_Actual--;
    this.mostrar();
  }

  alerta(tipo : any, mensaje : any, titulo : any){
    Swal.fire({
      title: titulo,
      text: mensaje,
      icon: tipo
    });
  }

  pais(pais: String): String {
    switch (pais) {
      case "Colombia":
        return "1";

      case "Estados unidos":
        return "2";

      default:
        return "";
    }
  }

  tipo_documento(td: String): String {
    switch (td) {
      case "Cedula de ciudadania":
        return "1";

      case "Cedula de extranjeria":
        return "2";

      case "Pasaporte":
        return "3";

      case "Permiso especial":
        return "4";

      default:
        return "";
    }
  }

  area(area: String): String {
    switch (area) {
      case "Administracion":
        return "1";

      case "Financiera":
        return "2";

      case "Compras":
        return "3";

      case "Infraestructura":
        return "4";

      case "Operacion":
        return "5";

      case "Talento humano":
        return "6";

      case "Servicios varios":
        return "7";

      default:
        return "";
    }
  }
}
