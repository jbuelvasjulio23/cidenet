import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RegistroServiceService } from '../../servicios/registro-service.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {
  empleado: FormGroup;
  fecha_actual: Date;
  habilitar : Boolean;

  constructor(private fb: FormBuilder, private resgistroService: RegistroServiceService) {
    this.fecha_actual = new Date();
    this.habilitar = false;

    this.empleado = this.fb.group({
      p_apellido: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern('[a-zA-Z]*')]],
      s_apellido: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern('[a-zA-Z]*')]],
      p_nombre: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern('[a-zA-Z]*')]],
      s_nombre: ['', [Validators.minLength(4), Validators.maxLength(50), Validators.pattern('[a-zA-Z ]*')]],
      pais: ['', [Validators.required]],
      area: ['', [Validators.required]],
      tipo_id: ['', [Validators.required]],
      n_identificacion: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20), Validators.pattern('[a-zA-Z0-9]*')]],
      f_ingreso: ['02/04/2021', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.empleado.get("pais")?.valueChanges.subscribe(x => {
    });
  }


  async save() {
    this.habilitar = true;
    var fecha = this.empleado.value.f_ingreso.split("/");
    var f = new Date(fecha[2], fecha[1] - 1, fecha[0]);
    var f2 = new Date();
    f2.setMonth(f2.getMonth() - 1);

    if (f > this.fecha_actual) {
      this.alerta("error","La fecha no puede ser mayor a la actual", "Registro");
    } else if (f <= this.fecha_actual && f > f2) {
      if (this.empleado.dirty && this.empleado.valid) {
        const empleado = {
          id: "",
          p_nombre: this.empleado.value.p_nombre.toUpperCase(),
          s_nombre: this.empleado.value.s_nombre.toUpperCase(),
          p_apellido: this.empleado.value.p_apellido.toUpperCase(),
          s_apellido: this.empleado.value.s_apellido.toUpperCase(),
          pais: this.empleado.value.pais + "",
          t_documento: this.empleado.value.tipo_id + "",
          n_documento: this.empleado.value.n_identificacion.toUpperCase(),
          email: "",
          f_ingreso: this.empleado.value.f_ingreso,
          area: this.empleado.value.area + "",
          estado: "",
          f_hora_registro: ""
        }
        let response : any = await this.resgistroService.crearEmpleado(empleado).toPromise();
        var msj = response.email;
        if(msj == "id"){
          this.alerta("error","El tipo de documento y numero en uso", "Registro");
        }else if(msj.substring(0,5) == "error"){
          this.alerta("error",msj, "Registro");
        }else{
          this.alerta("success","Se ha guardado la informacion del empleado, email: "+msj, "Registro");
        }
      }

    } else {
      this.alerta("error","la fecha no puede superar un mes de antiguedad", "Registro");
    }
    this.habilitar = false;
  }

  alerta(tipo : any, mensaje : any, titulo : any){
    Swal.fire({
      title: titulo,
      text: mensaje,
      icon: tipo
    });
  }


}
