import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Empleado } from '../interfaz/empleado'

@Injectable({
  providedIn: 'root'
})
export class RegistroServiceService {

  private api: String;

  constructor(private http: HttpClient) {
    this.api = 'http://localhost:8080/empleados';
  }

  crearEmpleado(emp: Empleado) {
    const path = `${this.api}/guardar`;
    return this.http.post(path, emp);
  }

  mostrarCantidad(emp: Empleado) {
    const path = `${this.api}/mostrarCantidad`;
    return this.http.post(path, emp);
  }

  mostrarEmpleado(emp: Empleado, prim: number, ult: number) {
    const path = `${this.api}/mostrar/${prim}/${ult}`;
    return this.http.post(path, emp);
  }

  eliminarEmpleado(id: String) {
    const path = `${this.api}/borrar/${id}`;
    return this.http.delete(path);
  }

  editarEmpleado(emp: Empleado) {
    const path = `${this.api}/editar`;
    return this.http.post(path, emp);
  }
}
